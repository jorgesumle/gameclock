#!/usr/bin/python

import unittest
import pygame
import sys

class MixerTest(unittest.TestCase):

    def setUp(self):
        pygame.init()
        #import pdb; pdb.set_trace()
        pygame.mixer.init()
        pass

    def test_stop(self):
        """this should not hang, but it does!"""
        with self.assertRaises(SystemExit):
            sys.exit()

    def test_something_else(self):
        """dumb entry just so the unit tests continue to demonstrate the above bug"""
        self.test_list_threads()

    def test_list_threads(self):
        """debug: list threads"""
        import threading
        for t in threading.enumerate():
            print t
